output "dev_vpc_id" {
  value = aws_vpc.development_vpc.id
}


output "dev_vpc_arn" {
  value = aws_vpc.development_vpc.arn
}


output "dev_subnet_1_id" {
  value = aws_subnet.dev_subnet_1.id
}


output "dev_subnet_1_arn" {
  value = aws_subnet.dev_subnet_1.arn
}
