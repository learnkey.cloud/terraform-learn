variable "vpc_cidr_block" {
  description = "cidr blocks vpc"
  type = string
  default = "10.0.0.0/16"
}


variable "subnet_cidr_block" {
  description = "subnet blocks vpc"
  type = string
  default = "10.0.10.0/24"
}


variable "environment" {
  description = "deployment environment"
}
