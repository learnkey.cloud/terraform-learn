#To run the terraform script with this variable file, run the command
#terraform apply -var-file [NAME_OF_TERRAFORM_VARIABLE_FILE].tfvars

#1 How to set which aws profile terraform should use: https://registry.terraform.io/providers/hashicorp/aws/latest/docs#shared-configuration-and-credentials-files

#2 More info no how to set AWS env varibales: https://registry.terraform.io/providers/hashicorp/aws/latest/docs#environment-variables

#3 Another was to set your own custom glocab variables: https://www.terraform.io/cli/config/environment-variables
provider "aws" {
  region     = "us-east-1"
  profile = "olaaws"
}



resource "aws_vpc" "development_vpc" {
  cidr_block       = var.vpc_cidr_block
  tags = {
    Name = var.environment
  }
}


resource "aws_subnet" "dev_subnet_1" {
  vpc_id     = aws_vpc.development_vpc.id
  cidr_block = var.subnet_cidr_block
  availability_zone = "us-east-1a"
  tags = {
    Name = var.environment
  }
}
